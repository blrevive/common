#!/bin/sh

# use cpm cache
export CPM_SOURCE_CACHE="$CI_PROJECT_DIR/.cpm-cache"
# use local cmake install prefix
export CMAKE_INSTALL_PREFIX="$CI_PROJECT_DIR/install-cache"
# export blrevive version for cmake scripts
export BLREVIVE_VERSION=$(cat $CI_PROJECT_DIR/blrevive.version)

BLRE_PKG_REGISTRY_URI="$CI_API_V4_URL/projects/BLRevive%2FBLRevive/packages/generic"
INSTALL_CACHE_PKG_REGISTRY_URI="$BLRE_PKG_REGISTRY_URI/install-cache/$BLREVIVE_VERSION/install-cache.tar.gz"

function exit_on_error() {
    if [ "$?" -ne 0 ]; then
        echo "[BLRE/BUILD] $1"
        exit 1
    fi
}

cd $CI_PROJECT_DIR
echo "[BLRE/BUILD] using BLRevive@$BLREVIVE_VERSION"

# pull install cache from generic package registry if building a module
if [ "$CI_PROJECT_NAME" != "blrevive" ]; then
    echo "[BLRE/BUILD] pulling install cache from $INSTALL_CACHE_PKG_REGISTRY_URI"
    curl -O "$INSTALL_CACHE_PKG_REGISTRY_URI"
    tar -xzvf install-cache.tar.gz
    rm install-cache.tar.gz

    export CPM_USE_LOCAL_PACKAGES="ON"
fi

# generate makefiles
echo "[BLRE/BUILD] generating makefiles for $CI_PROJECT_NAME"
cmake -B build -DBLRE_INSTALL_TO_BLR=OFF -DCMAKE_BUILD_TYPE=Release -DBLREVIVE_INSTALL=ON -DCMAKE_INSTALL_PREFIX="$CMAKE_INSTALL_PREFIX"
exit_on_error "error while configuring $CI_PROJECT_NAME"

# build module
echo "[BLRE/BUILD] building $CI_PROJECT_NAME (jobs: $(nproc --all))"
cmake --build build -j $(nproc --all)
exit_on_error "failed to build $CI_PROJECT_NAME"

if [ "$CI_PROJECT_NAME" == "blrevive" ]; then
    echo "[BLRE/BUILD] compiling loader"
    cd ./loader/
    cmake -B build -DCMAKE_BUILD_TYPE=Release
    exit_on_error "failed to configure loader"
    cmake --build build
    exit_on_error "failed to build loader"
    cd ..

    echo "[BLRE/BUILD] generating install cache"
    cmake --install build
    tar -czvf install-cache.tar.gz "./install-cache"

    echo "[BLRE/BUILD] pushing install cache to $INSTALL_CACHE_PKG_REGISTRY_URI"
    curl -sS -f --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file "./install-cache.tar.gz" $INSTALL_CACHE_PKG_REGISTRY_URI
fi