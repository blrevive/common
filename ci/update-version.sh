#!/bin/sh

NEXT_VERSION=$1
MODULE_INFO="$CI_PROJECT_DIR/source/module.json"
RELEASERC_FILE="$CI_PROJECT_DIR/.releaserc.json"

if [ "$CI_PROJECT_NAME" == "blrevive" ]; then
    ASSETS_STR="\"assets\": [{\"path\": \"doc/Changelog.md\", \"label\": \"Changelog\"}, {\"path\": \"build/$MODULE_NAME.dll\", \"label\": \"$MODULE_NAME-$NEXT_VERSION.dll\"}, {\"path\": \"loader/build/DINPUT8.dll\", \"label\": \"DINPUT8-$NEXT_VERSION.dll\"}]"
else
    ASSETS_STR="\"assets\": [{\"path\": \"doc/Changelog.md\", \"label\": \"Changelog\"}, {\"path\": \"build/$MODULE_NAME.dll\", \"label\": \"$MODULE_NAME-$NEXT_VERSION.dll\"}]"
fi

# replace release assets in releasrc
sed -i 's@\"assets\": \[\]@'"$ASSETS_STR"'@' "$RELEASERC_FILE"

# replace the version in the module.json
jq --arg next_version "$NEXT_VERSION" '.Version = $next_version' "$MODULE_INFO" > module.json.tmp && mv module.json.tmp "$MODULE_INFO"


# update BLRevive dep version if building module
if [ ! "$CI_PROJECT_NAME" == "blrevive" ]; then
    BLRE_LATEST_TAG="null"
    BLRE_PROJECT_ID="blrevive%2Fblrevive"
    BLRE_PROJECT_URL="https://gitlab.com/api/v4/projects/$BLRE_PROJECT_ID"

    # if were building on beta branch, fetch latest beta tag of BLRevive otherwise use main tag
    if [ "$CI_COMMIT_BRANCH" == "beta" ]; then
        BLRE_LATEST_TAG=$(curl -s "$BLRE_PROJECT_URL/repository/tags" | jq -r '[.[] | select(.name|test(".beta."))][0].name')
    else
        BLRE_LATEST_TAG=$(curl -s "$BLRE_PROJECT_URL/repository/tags" | jq -r '[.[] | select(.name|test(".beta.")|not)][0].name')
    fi

    # check if there is a tag
    if [ "$BLRE_LATEST_TAG" == "null" ]; then
        echo "[blre/common/updated] failed to retrieve latest tag of BLRevive for branch $CI_COMMIT_BRANCH"
        exit 1
    fi

    # replace the BLRevive version with the latest tag
    jq --arg blre_ver "${BLRE_LATEST_TAG:1}" '.Requires.BLRevive = ">=" + $blre_ver' "$MODULE_INFO" > module.json.tmp && mv module.json.tmp "$MODULE_INFO"

    # save blre version for further jobs
    echo ${BLRE_LATEST_TAG:1} > "$CI_PROJECT_DIR/blrevive.version"
else
    echo $NEXT_VERSION > "$CI_PROJECT_DIR/blrevive.version"
fi

